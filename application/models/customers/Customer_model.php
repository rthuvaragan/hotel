<?php

class Customer_model extends CI_Model{

    var $id;
    var $name;
    var $email;
    var $phone;
    var $passportno;
    var $roomtype;
    var $noofrooms;
    var $price;
    var $datein;
    var $dateend;
    var $message;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPassportno()
    {
        return $this->passportno;
    }

    /**
     * @param mixed $passportno
     */
    public function setPassportno($passportno)
    {
        $this->passportno = $passportno;
    }

    /**
     * @return mixed
     */
    public function getRoomtype()
    {
        return $this->roomtype;
    }

    /**
     * @param mixed $roomtype
     */
    public function setRoomtype($roomtype)
    {
        $this->roomtype = $roomtype;
    }

    /**
     * @return mixed
     */
    public function getNoofrooms()
    {
        return $this->noofrooms;
    }

    /**
     * @param mixed $noofrooms
     */
    public function setNoofrooms($noofrooms)
    {
        $this->noofrooms = $noofrooms;
    }

    /**
     * @return mixed
     */
    public function getDatein()
    {
        return $this->datein;
    }

    /**
     * @param mixed $datein
     */
    public function setDatein($datein)
    {
        $this->datein = $datein;
    }

    /**
     * @return mixed
     */
    public function getDateend()
    {
        return $this->dateend;
    }

    /**
     * @param mixed $dateend
     */
    public function setDateend($dateend)
    {
        $this->dateend = $dateend;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

}