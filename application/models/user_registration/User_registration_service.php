<?php

class User_registration_service extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function get_all_data($advertisement_model)
    {
        $this->db->select('*');
        $this->db->from('customers');
        $query = $this->db->get();
        return $query->result();
    }

    function customer_delete($customer_model)
    {
        $this->db->where(array(
            'id' => $customer_model->getId(),
        ));
        return  $this->db->delete('customers');
    }

    function get_customer_by_id($customer_model)
    {
        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('id', $customer_model->getId());
        $query = $this->db->get();
        return $query->row();
    }

    function customer_edit_update($customer_model)
    {
        $data = array(
            'name' => $customer_model->getName(),
            'email' => $customer_model->getEmail(),
            'phone' => $customer_model->getPhone(),
            'passportno' => $customer_model->getPassportno(),
            'price' => $customer_model->getPrice(),
            'roomtype' => $customer_model->getRoomtype(),
            'noofrooms' => $customer_model->getNoofrooms(),
            'datein' => $customer_model->getDatein(),
            'dateend' => $customer_model->getDateend(),
            'message' => $customer_model->getMessage(),
        );
        $this->db->where('id', $customer_model->getId());
        return  $this->db->update('customers', $data);
    }


}