<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class booking extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->model('users_model', 'users');
       // $this->load->model('Banking_model', 'users');
        $this->load->database();
        $this->load->helper('security');
    }


    function index()
    {
        //set validation rules
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|callback_alpha_space_only');
        $this->form_validation->set_rules('email', 'Emaid ID', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('passportno', 'Passport No', 'trim|required');
        $this->form_validation->set_rules('roomtype', 'Room Type');
        $this->form_validation->set_rules('noofrooms', 'No of Room');
        $this->form_validation->set_rules('price', 'price');
        $this->form_validation->set_rules('datein', 'Date In', 'trim|required');
        $this->form_validation->set_rules('dateend', 'Date End', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

        //run validation on post data
        if ($this->form_validation->run() == FALSE) {

            $session_info = $this->session->userdata['user_session_info'];
            $this->data['user_information'] = $this->users->get_user($session_info['session_id']);

            //validation fails
            $this->load->view('profile', $this->data);
        } else {
            //insert the contact form data into database
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'passportno' => $this->input->post('passportno'),
                'roomtype' => $this->input->post('roomtype'),
                'noofrooms' => $this->input->post('noofrooms'),
                'price' => $this->input->post('price'),
                'datein' => $this->input->post('datein'),
                'dateend' => $this->input->post('dateend'),
                'message' => $this->input->post('message')
            );

            if ($this->db->insert('customers', $data)) {
                // sucess message for when you submit 
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Thank you, Suceessfully Booked.</div>');
                redirect('booking/index');


            } else {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Some Error.  Please try again later!!!</div>');
                redirect('booking/index');
            }
        }
    }


    public function profile()
    {
        if ($this->session->userdata['user_session_info']) :
            $session_info = $this->session->userdata['user_session_info'];
            $this->data['user_information'] = $this->users->get_user($session_info['session_id']);
            $this->load->view('profile', $this->data);

        else :
            $this->session->set_flashdata('error', 'Please login before access this page.');
            redirect(site_url('profile'));
        endif;
    }


    //logout function
    public function logout()
    {
        $this->session->unset_userdata('user_session_info');
        //logout  flash message 
        $this->session->set_flashdata('message', 'Suceessfully , you logout now.');
        //redirect page login page
        redirect(site_url('users/login'));

    }


    //custom callback to accept only alphabets and space input
    function alpha_space_only($str)
    {
        if (!preg_match("/^[a-zA-Z ]+$/", $str)) {
            $this->form_validation->set_message('alpha_space_only', 'The %s field must contain only alphabets and space');
            return FALSE;
        } else {
            return TRUE;
        }
    }


}

?>