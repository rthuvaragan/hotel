<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->model('admin_model', 'admins'); // users is users_model alias name
        $this->load->database();
        $this->load->helper('security');

    }


    public function index()
    {
        $this->load->view('layout/header');
        //$this->load->view('customer/index');
        $this->load->view('layout/footer');
        // $this->load->view('signin_form');

    }

    //My booking details
    public function admin()
    {
        $this->load->view('admin_view');
    }


    public function login()
    {
        // set form validation emali id and password
        $this->form_validation->set_rules('email', 'email id ', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if ($this->form_validation->run() == FALSE):

            //if validation fail  redirect to same page as sigin_form
            $this->load->view('admin_view');
        else :
            $email = $this->input->post('email');
            //$password = md5($this->input->post('password'));
            $password = $this->input->post('password');
            $log_users = $this->admins->admin_login($email, $password);
            if (is_object($log_users)) :
                $session_data = array('user_name' => $log_users->name, 'session_id' => $log_users->id);
                $this->session->set_userdata('user_session_info', $session_data);
                redirect(site_url('users/display'));
            else :
                $this->session->set_flashdata('error', 'Invalid username or password.');
                $this->load->view('admin_view');
            endif;
        endif;
    }

      public function logout()
    {
        $this->session->unset_userdata('user_session_info');
        //logout message 
        $this->session->set_flashdata('message', 'Suceessfully , you logout now.');
        //redirect page login page
        redirect(site_url('admin/login'));

    }

}