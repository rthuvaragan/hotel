<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->model('users_model', 'users'); // users is users_model alias name
        $this->load->database();
        $this->load->helper('security');

        $this->load->model('customers/customer_model');
        $this->load->model('customers/customer_service');

        $this->load->model('user_registration/User_registration_model');
        $this->load->model('user_registration/User_registration_service');

    }


    public function index()
    {
        $this->load->view('layout/header');
        //$this->load->view('customer/index');
        $this->load->view('layout/footer');


    }

    //contact_us function
    public function contact_us()
    {
        $this->load->view('contact_us');

    }

    public function index1()
    {
        //set validation rules for contact_us form
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|callback_alpha_space_only');
        $this->form_validation->set_rules('email', 'Emaid ID', 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
        $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

        //run validation on post data
        if ($this->form_validation->run() == FALSE) {
            //validation fails view page in contact_us
            $this->load->view('contact_us');
        } else {
            //insert the contact form data into database
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('message')
            );

            if ($this->db->insert('contacts', $data)) {
                // success message
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">We received your message! Will get back to you shortly!!!</div>');
                redirect('users/index1');
            } else {
                // error message
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">sorry! Some Error.  Please try again later!!!</div>');
                redirect('users/index1');
            }
        }
    }


    //custom callback to accept only alphabets and space input
    public function alpha_space_only($str)
    {
        if (!preg_match("/^[a-zA-Z ]+$/", $str)) {
            $this->form_validation->set_message('alpha_space_only', 'The %s field must contain only alphabets and space');
            return FALSE;
        } else {
            return TRUE;
        }
    }


    // image gallery load view details
    public function image_gallery()
    {
        $this->load->view('image_gallery');
    }


    //My booking details load view details
    public function my_booking()
    {
        $this->load->view('my_booking');
    }


    //My booking details
    public function admin()
    {
        $this->load->view('admin_view');
    }

    //administration customers details
    public function administration()
    {
        $this->load->view('process');
    }


    public function login()
    {
        // set form validation emali id and password
        $this->form_validation->set_rules('email', 'email id ', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if ($this->form_validation->run() == FALSE):

            //if validation fail  redirect to same page as sigin_form
            $this->load->view('signin_form');
        else :
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $log_users = $this->users->user_login($email, $password);
            if (is_object($log_users)) :
                $session_data = array('user_name' => $log_users->name, 'session_id' => $log_users->id);
                $this->session->set_userdata('user_session_info', $session_data);
                redirect(site_url('users/profile'));
            else :
                $this->session->set_flashdata('error', 'Invalid username or password.');
                $this->load->view('signin_form');
            endif;
        endif;
    }

    public function signup()
    {
        //signup form validation
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('new_password', 'Password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation',
            'trim|required|matches[new_password]');
        $this->form_validation->set_rules('user_email', 'Email', 'required|is_unique[user_registration.email]');
        //email  address should  must be unique 
        $this->form_validation->set_message('is_unique', 'The %s is already taken');

        if ($this->form_validation->run() == FALSE):
            $this->load->view('signin_form');
        else :
            $data = array(
                'name' => $this->input->post('username'),
                'email' => $this->input->post('user_email'),
                'password' => md5($this->input->post('new_password')),
            );
            //if successfully insert the data
            $this->db->insert('user_registration', $data);
            unset($_POST);
            $this->session->set_flashdata('message', 'Suceessfully register, Please login now.');
            redirect(site_url('users/login'));
        endif;
    }

    public function profile()
    {
        if ($this->session->userdata['user_session_info']) :
            $session_info = $this->session->userdata['user_session_info'];
            $this->data['user_information'] = $this->users->get_user($session_info['session_id']);
            $this->load->view('profile', $this->data);
        // echo '<pre>'; print_r($this->data['user_information']); exit;
        else :
            $this->session->set_flashdata('error', 'Please login before access this page.');
            redirect(site_url('users'));
        endif;
    }


    // ===============Administration page process=========================

    public function process()
    {
        if ($this->session->userdata['user_session_info']) :
            $session_info = $this->session->userdata['user_session_info'];
            $this->data['user_information'] = $this->users->get_user($session_info['session_id']);
            $this->load->view('process', $this->data);
        // echo '<pre>'; print_r($this->data['user_information']); exit;
        else :
            $this->session->set_flashdata('error', 'Please login before access this page.');
            redirect(site_url('process'));
        endif;
    }

    public function display()
    {
        //method
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $data['all_customer'] = $customer_service->get_all_data($customer_model);

//        $partials = array('content' => 'process');
//        $this->template->load('template', $partials, $data);
        $this->load->view('process', $data);
    }

    //booking depend on the customer email id
    public function bookingView()
    {

        $email = $this->input->post('email');
        //method customer service
        $customer_service = new Customer_service();

        $data['all_customer'] = $customer_service->get_customer_data($email);


        $this->load->view('customer_booking', $data);
    }

    //delete
    function customerDelete($customer_id)
    {
        //new command for customer meodel
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $customer_model->setId($customer_id);

        if ($customer_service->customer_delete($customer_model) == 1) {
            redirect('customer');
        } else {
            redirect('customer');
        }
    }

    //booking cancellation  $customer_id
    function customerBookingDelete($customer_id)
    {
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $customer_model->setId($customer_id);

        if ($customer_service->customer_delete($customer_model) == 1) {
            $this->load->view('my_booking');
        } else {
            $this->load->view('my_booking');
        }
    }

    //Customer EditView   $customer_id
    function customerEditView($customer_id)
    {
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $customer_model->setId($customer_id);

        $data ['customer'] = $customer_service->get_customer_by_id($customer_model);

        $this->load->view('customer_edit', $data);

    }

    function customerBookingEditView($customer_id)
    {
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $customer_model->setId($customer_id);

        $data ['customer'] = $customer_service->get_customer_by_id($customer_model);

        $this->load->view('customer_booking_edit', $data);

    }
    //update booking EditViewUpdate   $customer_id
    function customerBookingEditViewUpdate($customer_id)
    {
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $customer_model->setId($customer_id);
        $customer_model->setName(trim($this->input->post('name')));
        $customer_model->setEmail(trim($this->input->post('email')));
        $customer_model->setPhone(trim($this->input->post('phone')));
        $customer_model->setPassportno(trim($this->input->post('passportno')));
        $customer_model->setRoomtype(trim($this->input->post('roomType')));
        $customer_model->setNoofrooms(trim($this->input->post('roomNo')));
        $customer_model->setPrice(trim($this->input->post('price')));
        $customer_model->setDatein(trim($this->input->post('datein')));
        $customer_model->setDateend(trim($this->input->post('dateend')));
        $customer_model->setMessage(trim($this->input->post('message')));

        if ($customer_service->customer_edit_update($customer_model) == 1) {
            $this->load->view('my_booking');
        } else {
            $this->load->view('my_booking');
        }
    }

    //customer edit view for their update
    function customerEditViewUpdate($customer_id)
    {
        $customer_model = new Customer_model();
        $customer_service = new Customer_service();

        $customer_model->setId($customer_id);
        $customer_model->setName(trim($this->input->post('name')));
        $customer_model->setEmail(trim($this->input->post('email')));
        $customer_model->setPhone(trim($this->input->post('phone')));
        $customer_model->setPassportno(trim($this->input->post('passportno')));
        $customer_model->setRoomtype(trim($this->input->post('roomType')));
        $customer_model->setNoofrooms(trim($this->input->post('roomNo')));
        $customer_model->setPrice(trim($this->input->post('price')));
        $customer_model->setDatein(trim($this->input->post('datein')));
        $customer_model->setDateend(trim($this->input->post('dateend')));
        $customer_model->setMessage(trim($this->input->post('message')));

        if ($customer_service->customer_edit_update($customer_model) == 1) {
            redirect('customer');
        } else {
            redirect('customer');
        }
    }
    //logout 
    public function logout()
    {
        $this->session->unset_userdata('user_session_info');
        //logout message 
        $this->session->set_flashdata('message', 'Suceessfully , you logout now.');
        //redirect page login page
        redirect(site_url('users/login'));

    }


}


