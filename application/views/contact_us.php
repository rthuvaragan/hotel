<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">

    <title>Contact_us</title>

    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contentCollapse.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-social.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>asseets/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">

      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>

</head>
<body>
<!-- start of Navigation Bar -->

<nav class="navbar navbar-inverse navbar-static-top on margin" role="Navigation">
    <div class="container-fluid">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>

            <a href="#" class="navbar-brand">Hotel</a>
        </div>
        <div id="myNavbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                <li><a href="<?php echo base_url('users/contact_us'); ?>"><i class="glyphicon glyphicon-briefcase"></i>
                        Contact Us</a></li>
                <li><a href="<?php echo base_url('users/image_gallery'); ?>"><i class="glyphicon glyphicon-picture"></i>
                        Image Gallery</a></li>
                <li><a href="<?php echo base_url('users/my_booking'); ?>"><i class="glyphicon glyphicon-book"></i> My
                        Booking</a></li>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo base_url('users/login') ?>"><i class="glyphicon glyphicon-log-in"></i> Login</a>
                </li>
                <li><a href="<?php echo base_url('users/signup') ?>"><i class="glyphicon glyphicon-user"></i> Sign
                        Up</a></li>
            </ul>
        </div>
    </div>

</nav>
<!-- End Navigation Bar -->

<!-- Start of jumbotron -->
<!--<div class="jumbotron">-->
<div class="container">

</div>
<!-- End jumbotron -->


<!-- start of Website Mani Content -->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4>Contact Us</h4>
            <!--  -->
            <div class="container content">


                <div class="row">
                    <img src="<?php echo base_url(); ?>assets/images/contact_us.jpg"/>
                    <!-- <img src="http://images.forbes.com/media/2011/02/10/0210_happy-careers-customer-service_485x340.jpg" class="span4" /> -->

                    <p class="span3"><br><br>
                        36/54, Havelock Road,<br>
                        Wellawatta,<br>
                        Colombo 06.<br>
                        Sri Lanka.<br>
                        Tel: 0112546875.<br>
                        Email : infohotel@gmail.com
                    </p>

                </div>
            </div>
            <!--  -->

        </div>
        <div class="col-md-4">
            <h4></h4>
            <p class="p"><b>Hotel</b> is committed to providing our customers with the lowest available prices. With us,
                hotel booking has never been so simple and effective. We provide users with a friendly interface,
                efficient booking system and high quality customer service. We guarantee you will be satisfied through
                booking your next hotel with us. </p>

            <!-- google map -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15844.245738738813!2d79.86862536737229!3d6.883244420416692!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2slk!4v1478882385569"
                    width="375" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- google map -->



        </div>


        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Contact Us</h3>
                </div>
                <div class="panel-body">
                    <?php $attributes = array("name" => "contact_us");

                    echo form_open("users/index1", $attributes); ?>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" name="name" placeholder="Your Full Name" type="text"
                               value="<?php echo set_value('name'); ?>"/>
                        <span class="text-danger"><?php echo form_error('name'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="email">Email ID</label>
                        <input class="form-control" name="email" placeholder="Email-ID" type="text"
                               value="<?php echo set_value('email'); ?>"/>
                        <span class="text-danger"><?php echo form_error('email'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input class="form-control" name="subject" placeholder="Subject" type="text"
                               value="<?php echo set_value('subject'); ?>"/>
                        <span class="text-danger"><?php echo form_error('subject'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" name="message" rows="4"
                                  placeholder="Message"><?php echo set_value('message'); ?></textarea>
                        <span class="text-danger"><?php echo form_error('message'); ?></span>
                    </div>

                    <div class="form-group">
                        <button name="submit" type="submit" class="btn btn-success">Submit</button>
                    </div>

                    <?php echo form_close(); ?>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<hr/>
<p align="center">Geo Location</p>
<hr/>
<div id="map"></div>
<!-- Start of the footer -->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4>Contact Address</h4>
                <address>
                    36/54, Havelock Road,<br>
                    Wellawatta,<br>
                    Colombo 06.<br>
                    Sri Lanka.<br>
                    Tel: 0112546875.<br>
                    Email : infohotel@gmail.com
                </address>
            </div>


            <div class="col-md-3">
                <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blog</h4>
                <ul class="site-footer-nav">
                    <li><a href="">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contacts</a></li>
                    <li><a href="">Links</a></li>
                </ul>
            </div>


            <div class="col-md-3">
                <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;About Us</h4>
                <ul class="site-footer-nav">
                    <li><a href="">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contacts</a></li>
                    <li><a href="">Links</a></li>
                </ul>
            </div>

            <div class="col-md-3">
                <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Social Media</h4>

                <ul class="site-footer-nav">
                    <li><a href=""><i class="fa fa-facebook-square fa-lg"></i> Facebook</a></li>
                    <li><a href=""><i class="fa fa-twitter-square fa-lg"></i> Twiter</a></li>
                    <li><a href=""><i class="fa fa-youtube-square fa-lg"></i> Youtube</a></li>
                    <li><a href=""><i class="fa fa-google-plus-square fa-lg"></i> Google plus</a></li>
                </ul>
            </div>
        </div>


        <div class="bottom-footer">
            <div class="col-md-5">Copyright &copy;2017 Developed By R.Thuvaragan.</div>
            <div class="col-md-7">
                <ul class="footer-nav">
                    <li><a href="">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contacts</a></li>
                    <li><a href="">Links</a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<!-- End of the Footer -->

<script type="text/javascript" src="http://localhost:/Coursework/assets/js/jquery.js"></script>
<script src="http://localhost:/Coursework/assets/js/bootstrap.min.js"></script>


<script type="text/javascript" src="<?php echo base_url("assets/js/jQuery-1.10.2.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
 <script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 6
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaH2A1PTc-LwvIeyrA4IsXbR9At-oW8LQ&callback=initMap">
    </script>
</body>
</html>