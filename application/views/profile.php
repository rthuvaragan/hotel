
<!DOCTYPE html>
<html>
<head>
    <title>welcome</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	
	
	<!--<meta charset="utf-8">-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contentCollapse.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-social.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>






</head>
<body>
<!-- start of Navigation Bar -->

<nav class="navbar navbar-inverse navbar-static-top on margin" role="Navigation">
<div class="container-fluid">

		<div class="navbar-header">

		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>

		</button>

		<a href="#" class="navbar-brand">Hotel</a>
		</div>
	<div id="myNavbar" class="collapse navbar-collapse">
		<ul class="nav navbar-nav navbar-left">
                    <li class="active" ><a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-home" ></i> Home</a></li>
					<li><a href="<?php echo base_url('users/contact_us'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Contact Us</a></li>
					<li><a href="<?php echo base_url('users/image_gallery'); ?>"><i class="glyphicon glyphicon-picture"></i> Image Gallery</a></li>
					<li><a href="<?php echo base_url('users/my_booking'); ?>"><i class="glyphicon glyphicon-book"></i> My Booking</a></li>
					</ul>


					<ul class="nav navbar-nav navbar-right">
					    <li>Hi...:<?= $user_information->name; ?></li>  
					<li><a href="<?=site_url('users/logout')?>"><i class="glyphicon glyphicon-user"></i> logout</a></li>
					</ul>
	</div>			
		</div>

	</nav>
<!-- End Navigation Bar -->

 
 <h3><b> Welcome to Hotel Booking </b></h3><br> 

<!-- start of Website Mani Content -->
<div class="container">
	<div class="row">
	<div class="col-md-4">

		<!-- success message -->
			<?php echo form_close(); ?>
            <?php echo $this->session->flashdata('msg'); ?>
        <!-- success message -->

		<div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Welcome to Booking Details</h3>
        </div>
        <div class="panel-body">
            <?php $attributes = array("name" => "booking");
            echo form_open("booking/index", $attributes);?>
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" name="name" placeholder="Your Full Name" autocomplete="off" type="text" value="<?php echo $user_information->name; ?>" />
                <span class="text-danger"><?php echo form_error('name'); ?></span>
            </div>
            
            <div class="form-group">
                <label for="email">Email ID</label>
                <input class="form-control" name="email" placeholder="Email-ID" autocomplete="off" type="text" value="<?php echo $user_information->email; ?>" />
                <span class="text-danger"><?php echo form_error('email'); ?></span>
            </div>


            <!-- 1 -->
            <div class="form-group">
                <label for="phone">Phone</label>
                <input class="form-control" name="phone" placeholder="Your Phone Number" autocomplete="off" type="text" value="<?php echo set_value('phone'); ?>" />
                <span class="text-danger"><?php echo form_error('phone'); ?></span>
            </div>
            <!-- 1 -->


             <!-- 2 -->
            <div class="form-group">
                <label for="nationalid">Passport No</label>
                <input class="form-control" name="passportno" placeholder="Your Passport No" autocomplete="off" type="text" value="<?php echo set_value('passportno'); ?>" />
                <span class="text-danger"><?php echo form_error('passportno'); ?></span>
            </div>
            <!-- 2 -->


             <!-- Room Type -->
            <div class="form-group">
                <label for="roomtype">Room Type</label>
                <select name="roomtype" class="form-control">
                <option>Select Room type...</type>
                	<option>Medium</option>
                	<option>Normal</option>
                	<option>VIP</option>
                </select>
               
                <!-- <span class="text-danger"><?php echo form_error('roomtype'); ?></span> -->
            </div>
            <!-- Room type -->

            
            <!-- 4 -->
            <div class="form-group">
                <label for="noofrooms">No of Room/s</label>
                <select name="noofrooms" class="form-control">
                <option>Select the No of Room/s</type>
                	<option>01</option>
                	<option>02</option>
                	<option>04</option>
                	<option>05</option>
                	<option>06</option>
                	<option>07</option>
                	<option>08</option>
                	<option>09</option>
                	<option>10</option>
                </select>
                 <!-- <span class="text-danger"><?php echo form_error('noofrooms'); ?></span>  -->
            </div>
            <!-- 4 -->
         
         	 <div class="form-group">
                <label for="price">Room Price</label>
                <select name="price" class="form-control">
                 <option>Select the Price</type>
                	<option>$60</option>
                	<option>$75</option>
                	<option>$95</option>
                	<option>$120</option>

                </select>
               
            </div>

            <!-- Date In -->
            <div class="form-group">
                <label for="datein">Date In</label>
                <input class="form-control" name="datein" placeholder="Start Date" autocomplete="off" type="date" value="<?php echo set_value('datein'); ?>" />
                <span class="text-danger"><?php echo form_error('datein'); ?></span>
            </div>
            <!-- Date In -->


            <!-- Date Out-->
            <div class="form-group">
                <label for="dateend">Date Out</label>
                <input class="form-control" name="dateend" placeholder="End Date" autocomplete="off" type="date" value="<?php echo set_value('dateend'); ?>" />
                <span class="text-danger"><?php echo form_error('dateend'); ?></span>
            </div>
            <!-- Date Out-->

            <!-- Message  -->
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" name="message" rows="4" placeholder="Please Type your additional Information"><?php echo set_value('message'); ?></textarea>
                <span class="text-danger"><?php echo form_error('message'); ?></span>
            </div>
            <!-- Message -->


            <div class="form-group">
                <button name="submit" type="submit" class="btn btn-success">Submit</button>
            </div>
	 

            
            
       		 </div>
   		 </div>	

	</div>
	
		</div>
</div>

</div>





<!-- Start of the footer -->
<footer class="site-footer">
	<div class="container">
		<div class="row">
				<div class="col-md-3">
					<h4>Contact Address</h4>
						<address>
							36/54, Havelock Road,<br>
							Wellawatta,<br>
							Colombo 06.<br>
							Sri Lanka.<br>
							Tel: 0112546875.<br>
							Email : infohotel@gmail.com			
						</address>
				</div>


				<div class="col-md-3">
					<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blog</h4>
							<ul class="site-footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contacts</a></li>
								<li><a href="#">Links</a></li>
							</ul>
				</div>


				<div class="col-md-3">
					<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;About Us</h4>
							<ul class="site-footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contacts</a></li>
								<li><a href="#">Links</a></li>
							</ul>
				</div>

				<div class="col-md-3" >
				<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Social Media</h4>
					
							<ul class="site-footer-nav">
							<li><a href=""><i class="fa fa-facebook-square fa-lg"></i>  Facebook</a></li>
							<li><a href=""><i class="fa fa-twitter-square fa-lg"></i>  Twiter</a></li>
							<li><a href=""><i class="fa fa-youtube-square fa-lg"></i>  Youtube</a></li>
							<li><a href=""><i class="fa fa-google-plus-square fa-lg"></i>  Google plus</a></li>
							</ul>
				</div>
		</div>
	
		
				

			<div class="bottom-footer">
				<div class="col-md-5">Copyright &copy;2017 Developed By R.Thuvaragan.</div>
						<div class="col-md-7">
							<ul class="footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contacts</a></li>
								<li><a href="#">Links</a></li>
							</ul>
						</div>
				
			</div>
	</div>
</footer>
<!-- End of the Footer -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
<!--load jquery ui js file-->
<script src="<?php echo base_url(); ?>assets/jquery-ui-1.12.0/jquery-ui.js"></script>

</body>
</html>