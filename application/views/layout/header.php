
<!DOCTYPE html>
<html>
<head>
    <title>welcome</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	
	
	<!--<meta charset="utf-8">-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contentCollapse.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-social.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">





</head>
<body>
<!-- start of Navigation Bar -->

<nav class="navbar navbar-inverse navbar-static-top on margin" role="Navigation">
<div class="container-fluid">

		<div class="navbar-header">

		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>

		</button>

		<a href="#" class="navbar-brand">Hotel</a>
		</div>
	<div id="myNavbar" class="collapse navbar-collapse">
		<ul class="nav navbar-nav navbar-left">
                    <li class="active" ><a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-home" ></i> Home</a></li>
					<li><a href="<?php echo base_url('users/contact_us'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Contact Us</a></li>
					<li><a href="<?php echo base_url('users/image_gallery'); ?>"><i class="glyphicon glyphicon-picture"></i> Image Gallery</a></li>
					<li><a href="<?php echo base_url('users/my_booking'); ?>"><i class="glyphicon glyphicon-book"></i> My Booking</a></li>
					</ul>


					<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url('users/admin') ?>"><i class="glyphicon glyphicon-log-in"></i> Admin</a></li>
					<li><a href="<?php echo base_url('users/login') ?>"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
					<li><a href="<?php echo base_url('users/signup') ?>"><i class="glyphicon glyphicon-user"></i> Sign Up</a></li>
					</ul>
	</div>			
		</div>

	</nav>
<!-- End Navigation Bar -->

<!-- Start of jumbotron -->
<!--<div class="jumbotron">-->
<div class="container-fluid">
<div id="My_carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
				<li data-target ="#My_carousel data-slide-to="0" class="active"></li>
				<li data-target ="#My_carousel data-slide-to="1"></li>
				<li data-target ="#My_carousel data-slide-to="2"></li>
				<li data-target ="#My_carousel" data-slide-to="3"></li>
				<li data-target ="#My_carousel" data-slide-to="4"></li>
				<li data-target ="#My_carousel" data-slide-to="5"></li>
				<li data-target ="#My_carousel" data-slide-to="6"></li>
				<li data-target ="#My_carousel" data-slide-to="7"></li>
				<li data-target ="#My_carousel" data-slide-to="8"></li>
				<li data-target ="#My_carousel" data-slide-to="9"></li>
				<li data-target ="#My_carousel" data-slide-to="10"></li>
				<li data-target ="#My_carousel" data-slide-to="11"></li>
				<li data-target ="#My_carousel" data-slide-to="12"></li>
				<li data-target ="#My_carousel" data-slide-to="13"></li>
				<li data-target ="#My_carousel" data-slide-to="14"></li>
	</ol>
			<div class="carousel-inner" role="listbox">
				<div class="item active"><img src="<?php echo base_url(); ?>assets/images/100.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/200.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/300.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/400.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/500.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/600.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/700.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/800.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/900.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/1000.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/1100.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/1200.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/1300.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/1400.jpg" alt="thuva" width="100%" height="350px"></div>
					<div class="item"><img src="<?php echo base_url(); ?>assets/images/1500.jpg" alt="thuva" width="100%" height="350px"></div>


					<a class="left carousel-control" href="#My_carousel" role="button" data-slide="prev">
					
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#My_carousel" role="button" data-slide="next">
					
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
				
			</div>	

</div>
</div>
<!-- End jumbotron -->


<!-- start of Website Mani Content -->
<div class="container">
	
	<div class="col-md-4"></div>
</div>

<div class="container">
	<div class="row">
	<div class="col-md-4">

		<h4>.</h4>
		<a href='users/image_gallery'><img src="<?php echo base_url(); ?>assets/images/a.jpg"/></a>
	</div>


	<div class="col-md-4">
	<h4>.</h4>
		<a href='users/image_gallery'><img src="<?php echo base_url(); ?>assets/images/b.jpg"/></a>
		</div>
		

		<div class="col-md-4">
		<h4>.</h4>
		<a href='users/image_gallery'><img src="<?php echo base_url(); ?>assets/images/c.jpg"/></a>
		</div>
</div>
