
<!DOCTYPE html> 
<html lang="en"> 
  <head> <title>user login form</title>  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
  </head> 
    <body>
       
 <!-- start of Navigation Bar -->

<nav class="navbar navbar-inverse navbar-static-top on margin" role="Navigation">
<div class="container-fluid">

		<div class="navbar-header">

		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>

		</button>

		<a href="#" class="navbar-brand">Hotel</a>
		</div>
	<div id="myNavbar" class="collapse navbar-collapse">
		<ul class="nav navbar-nav navbar-left">
                                        <li class="active" ><a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-home" ></i> Home</a></li>
					<li><a href="<?php echo base_url('users/contact_us'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Contact Us</a></li>
					<li><a href="<?php echo base_url('users/image_gallery'); ?>"><i class="glyphicon glyphicon-picture"></i> Image Gallery</a></li>
          <li><a href="<?php echo base_url('users/my_booking'); ?>"><i class="glyphicon glyphicon-book"></i> My Booking</a></li>
					</ul>


					<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url('users/login') ?>"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
					<li><a href="<?php echo base_url('users/signup') ?>"><i class="glyphicon glyphicon-user"></i> Sign Up</a></li>

					</ul>
	</div>			
		</div>

	</nav>
 
<!-- End Navigation Bar -->
        
      
    <div class="container-fluid">
     <!--<div class="page_content bg_data">-->             
        <div class="row">    
           
         <div class="col-md-10 middle-col">
            
             <div class="row">
             <!-- Success Message -->
              <?php if($this->session->flashdata('message')){?>
                  <div class="alert alert-success fade in block-inner">            
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="icon-checkmark-circle"></i> <?php echo $this->session->flashdata('message')?>
                  </div>
              <?php } ?>
            <!-- Success Message -->
            
            <!-- Error Message -->
              <?php if($this->session->flashdata('error')){?>
                <div class="alert alert-danger fade in block-inner">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <i class="icon-checkmark-circle"></i> <?php echo $this->session->flashdata('error')?> 
                </div>
              <?php } ?>
            <!-- Error Message -->
            
                <div class="col-md-1">

                </div>
                <div class="col-md-5">
                <div class="modal-header">
                    
                  <!-- login form -->
                  <h2 class="modal-title"><i class="icon-paragraph-justify2"></i>Login Form </h2>
                  
                </div>                  
                  <form method="post" action="<?php echo site_url('users/login') ?>">
                      <h5>Email</h5>
                      <input type="text" placeholder="Enter your email ID" name="email" autocomplete="off" value="<?php echo set_value('email'); ?>" class="form-control" size="50" />
                      <div class="errorMessage"><?php echo form_error('email'); ?></div>
                      <h5>Password</h5>
                      <input type="password" placeholder="Enter your password" name="password" autocomplete="off" value="<?php echo set_value('password'); ?>" class="form-control" size="50" />
                      <div class="errorMessage"><?php echo form_error('password'); ?></div>                          
                      <div><br> 
                      <input type="submit" class="btn btn-primary" value="Login" />
                      </div> 
                    </div>
                  </form>

                <!-- New user registration form -->
                <div class="col-md-1"></div>                
                <div class="col-md-5">
                <div class="modal-header">

                  <h2 class="modal-title"><i class="icon-paragraph-justify2"></i>New user registration Form </h2>
                </div>
                    <form method="post" action="<?php echo site_url('users/signup') ?>">
                        <h5>Name</h5>
                        <input type="text" name="username" placeholder="Enter your name" autocomplete="off" value="<?php echo set_value('username'); ?>" class="form-control" size="50" />
                        <div class="errorMessage"><?php echo form_error('username'); ?></div>
                        <h5>Email Address</h5>
                        <input type="text"  name="user_email" placeholder="Enter your email" autocomplete="off" value="<?php echo set_value('user_email'); ?>" class="form-control" size="50" />
                        <div class="errorMessage"><?php echo form_error('user_email'); ?></div>
                        <h5>Password</h5>
                        <input type="password" name="new_password" placeholder="Enter your new password" autocomplete="off" value="<?php echo set_value('new_password'); ?>" class="form-control" size="50" />
                        <div class="errorMessage"><?php echo form_error('new_password'); ?></div>
                        <h5>Password Confirm</h5>
                        <input type="password" name="passconf" placeholder="Enter your confirm password" autocomplete="off" value="<?php echo set_value('passconf'); ?>" class="form-control" size="50" />
                        <div class="errorMessage"><?php echo form_error('passconf'); ?></div>
                        <div><br>
                        <input type="submit" class="btn btn-primary" value="Signup" />
                        </div>
                    </form>
                </div>
             </div><hr>
              
           </div>
            <div class="col-sm-2"></div>

         </div>
        </div>
         <!-- this for full width layout -->    
     </div>  
    </div>
    
       <!--footer start-->
       
       <footer class="site-footer">
	<div class="container">
		<div class="row">
				<div class="col-md-3">
					<h4>Contact Address</h4>
						<address>
							36/54, Havelock Road,<br>
              Wellawatta,<br>
              Colombo 06.<br>
              Sri Lanka.<br>
              Tel: 0112546875.<br>
              Email : infohotel@gmail.com 	
						</address>
				</div>


				<div class="col-md-3">
					<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blog</h4>
							<ul class="site-footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contacts</a></li>
								<li><a href="#">Links</a></li>
							</ul>
				</div>


				<div class="col-md-3">
					<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;About Us</h4>
							<ul class="site-footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contacts</a></li>
								<li><a href="#">Links</a></li>
							</ul>
				</div>

				<div class="col-md-3" >
				<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Social Media</h4>
					
							<ul class="site-footer-nav">
							<li><a href=""><i class="fa fa-facebook-square fa-lg"></i>  Facebook</a></li>
							<li><a href=""><i class="fa fa-twitter-square fa-lg"></i>  Twiter</a></li>
							<li><a href=""><i class="fa fa-youtube-square fa-lg"></i>  Youtube</a></li>
							<li><a href=""><i class="fa fa-google-plus-square fa-lg"></i>  Google plus</a></li>
							</ul>
				</div>
		</div>
	
		
				

			<div class="bottom-footer">
				<div class="col-md-5">Copyright &copy;2017 Developed By R.Thuvaragan.</div>
						<div class="col-md-7">
							<ul class="footer-nav">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Contacts</a></li>
								<li><a href="#">Links</a></li>
							</ul>
						</div>
				
			</div>
	</div>
</footer>
       <!--footer end-->
  </body>
</html>


