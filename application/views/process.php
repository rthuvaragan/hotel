<!DOCTYPE html>
<html>
<head>
    <title>welcome</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!--<meta charset="utf-8">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contentCollapse.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-social.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>


</head>
<body>
<!-- start of Navigation Bar -->

<nav class="navbar navbar-inverse navbar-static-top on margin" role="Navigation">
    <div class="container-fluid">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>

            <a href="#" class="navbar-brand">Hotel</a>
        </div>
        <div id="myNavbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="active"><a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-home"></i>
                        Home</a></li>
                <li><a href="<?php echo base_url('users/contact_us'); ?>"><i class="glyphicon glyphicon-briefcase"></i>
                        Contact Us</a></li>
                <li><a href="<?php echo base_url('users/image_gallery'); ?>"><i class="glyphicon glyphicon-picture"></i>
                        Image Gallery</a></li>
                <li><a href="<?php echo base_url('users/my_booking'); ?>"><i class="glyphicon glyphicon-book"></i> My
                        Booking</a></li>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                <!-- <li>Hi...:<?= $user_information->name; ?></li>   -->
                <li><a href="<?= site_url('admin/logout') ?>"><i class="glyphicon glyphicon-user"></i> logout</a></li>
            </ul>
        </div>
    </div>

</nav>
<!-- End Navigation Bar -->


<h1><b> Welcome to Administration </b></h1><br>

<!-- start of Website Mani Content -->
<div class="container">
    <div class="row">
        <div >


            <div class="panel panel-default">
                <div class="panel-heading">

                    <!--========================= ERROR -->

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
<!--                            <th>Customer ID</th>-->
                            <th>Custemer Name</th>
                            <th>Email ID</th>
                            <th>phone no</th>
                            <th>Passport No</th>
                            <th>Room Type</th>
                            <th>No of Rooms</th>
                            <th>Room Price</th>
                            <th>Date IN</th>
                            <th>Date Out</th>
                            <th>Message</th>
                            <th>ACTION Edit</th>
                            <th>ACTION Delete</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($all_customer AS $row) { ?>


                            <tr>
                                <td><?php echo $row->id ?></td>
                                <td><?php echo $row->name ?></td>
                                <td><?php echo $row->email ?></td>
                                <td><?php echo $row->phone ?></td>
                                <td><?php echo $row->passportno ?></td>
                                <td><?php echo $row->roomtype ?></td>
                                <td><?php echo $row->noofrooms ?></td>
                                <td><?php echo $row->price ?></td>
                                <td><?php echo $row->datein ?></td>
                                <td><?php echo $row->dateend ?></td>
                                <td><?php echo $row->message ?></td>
                                <td><a href="<?php echo base_url(); ?>index.php/customer-edit-view/<?php echo $row->id ?>" type="button" class="btn btn-default navbar-btn">Edit</a></td>
                                <td><a href="<?php echo base_url(); ?>index.php/customer-delete/<?php echo $row->id ?>" onclick="return confirm('Are you sure?');" type="button" class="btn btn-default navbar-btn">Delete</a></td>

<!--                                <td><a href='".site_url(' Administration/edit/'.$row->id)."'>Edit</a>-->
<!--                                <td><a href='".site_url(' Administration/delete/'.$row->id)."'>Delete</a>-->

                            </tr>

                        <?php } ?>


                        </tbody>
                    </table>

                </div>
            </div>

        </div>
        
        </div>
    </div>

</div>


<!-- Start of the footer -->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4>Contact Address</h4>
                <address>
                    36/54, Havelock Road,<br>
                    Wellawatta,<br>
                    Colombo 06.<br>
                    Sri Lanka.<br>
                    Tel: 0112546875.<br>
                    Email : infohotel@gmail.com
                </address>
            </div>


            <div class="col-md-3">
                <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Social Media</h4>
                <ul class="site-footer-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Links</a></li>
                </ul>
            </div>


            <div class="col-md-3">
                <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;About Us</h4>
                <ul class="site-footer-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Links</a></li>
                </ul>
            </div>

            <div class="col-md-3">
                <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blog </h1>

                    <ul class="site-footer-nav">
                        <li><a href=""><i class="fa fa-facebook-square fa-lg"></i> Facebook</a></li>
                        <li><a href=""><i class="fa fa-twitter-square fa-lg"></i> Twiter</a></li>
                        <li><a href=""><i class="fa fa-youtube-square fa-lg"></i> Youtube</a></li>
                        <li><a href=""><i class="fa fa-google-plus-square fa-lg"></i> Google plus</a></li>
                    </ul>
            </div>
        </div>


        <div class="bottom-footer">
            <div class="col-md-5">Copyright &copy;2017 Developed By R.Thuvaragan.</div>
            <div class="col-md-7">
                <ul class="footer-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Links</a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<!-- End of the Footer -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
<!--load jquery ui js file-->
<script src="<?php echo base_url(); ?>assets/jquery-ui-1.12.0/jquery-ui.js"></script>

</body>
</html>