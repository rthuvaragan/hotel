
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Image Gallery</title>
	
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contentCollapse.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-social.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asseets/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	 <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">



</head>
<body>
<!-- start of Navigation Bar -->
<nav class="navbar navbar-inverse navbar-static-top on margin" role="Navigation">
<div class="container-fluid">

		<div class="navbar-header">

		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>

		</button>

		<a href="#" class="navbar-brand">Hotel</a>
		</div>
	<div id="myNavbar" class="collapse navbar-collapse">
		<ul class="nav navbar-nav navbar-left">
                    <li> <a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-home" ></i> Home</a></li>
					<li><a href="<?php echo base_url('users/contact_us'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Contact Us</a></li>
					<li><a href="<?php echo base_url('users/image_gallery'); ?>"><i class="glyphicon glyphicon-picture"></i> Image Gallery</a></li>
					<li><a href="<?php echo base_url('users/my_booking'); ?>"><i class="glyphicon glyphicon-book"></i> My Booking</a></li>
					</ul>


					<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url('users/login') ?>"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
					<li><a href="<?php echo base_url('users/signup') ?>"><i class="glyphicon glyphicon-user"></i> Sign Up</a></li>
					</ul>
	</div>			
		</div>

	</nav>
<!-- End Navigation Bar -->

<!-- Start of jumbotron -->
<!--<div class="jumbotron">-->
<div class="container">

</div>
<!-- End jumbotron -->


<!-- start of Website Mani Content -->
<div class="container">
	<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>

			<?php echo form_open('customer-booking/') ?>
                      <h4>Booking ID.</h4>
                      <input required type="text" name="email" placeholder="Enter your Email ID" autocomplete="off" value="<?php echo set_value('email'); ?>" class="form-control" size="50" />
                     <span class="text-danger"><?php echo form_error('email'); ?></span>
                                          
                      <div><br> 

                      <input type="submit" class="btn btn-primary" value="Submit" />
                      </div> 
                    </div>
		<?php echo form_close() ?>
              </div>
</div>

</div>
</div>
<!-- Start of the footer -->




<footer class="site-footer">
	<div class="container">
		<div class="row">
				<div class="col-md-3">
					<h4>Contact Address</h4>
						<address>
							36/54, Havelock Road,<br>
							Wellawatta,<br>
							Colombo 06.<br>
							Sri Lanka.<br>
							Tel: 0112546875.<br>
							Email : infohotel@gmail.com				
						</address>
				</div>


				<div class="col-md-3">
					<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blog</h4>
							<ul class="site-footer-nav">
								<li><a href="">Home</a></li>
								<li><a href="">Blog</a></li>
								<li><a href="">Contacts</a></li>
								<li><a href="">Links</a></li>
							</ul>
				</div>


				<div class="col-md-3">
					<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;About Us</h4>
							<ul class="site-footer-nav">
								<li><a href="">Home</a></li>
								<li><a href="">Blog</a></li>
								<li><a href="">Contacts</a></li>
								<li><a href="">Links</a></li>
							</ul>
				</div>

				<div class="col-md-3" >
				<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Social Media</h4>
					
							<ul class="site-footer-nav">
							<li><a href=""><i class="fa fa-facebook-square fa-lg"></i>  Facebook</a></li>
							<li><a href=""><i class="fa fa-twitter-square fa-lg"></i>  Twiter</a></li>
							<li><a href=""><i class="fa fa-youtube-square fa-lg"></i>  Youtube</a></li>
							<li><a href=""><i class="fa fa-google-plus-square fa-lg"></i>  Google plus</a></li>
							</ul>
				</div>
		</div>
	
		
				

			<div class="bottom-footer">
				<div class="col-md-5">Copyright &copy;2017 Developed By R.Thuvaragan.</div>
						<div class="col-md-7">
							<ul class="footer-nav">
								<li><a href="">Home</a></li>
								<li><a href="">Blog</a></li>
								<li><a href="">Contacts</a></li>
								<li><a href="">Links</a></li>
							</ul>
						</div>
				
			</div>
	</div>
</footer>
<!-- End of the Footer -->

<script type="text/javascript" src="http://localhost:/Coursework/assets/js/jquery.js"></script>
<script src="http://localhost:/Coursework/assets/js/bootstrap.min.js"></script>
</body>
</html>
