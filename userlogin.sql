-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2017 at 02:05 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `error_solve`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'admin@gmail.com', '789456');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`) VALUES
(1, 'RajaratnamThuvaragan', 'rthuvaragan@gmail.com', 'hi ... Regariding my final project', 'Hi................'),
(2, 'Shan Raja', 'rajashan@gmail.com', 'hi  This is my final exam', 'hi'),
(3, 'safras', 'safras@yahoo.com', 'hi', 'hello'),
(4, 'perera', 'perera@yahoo.com', 'hello', 'hi hello'),
(5, 'thu', 'thu@gmail.com', 'hi', 'hin hii'),
(6, 'sunil', 'sunil@gmail.com', 'hi', 'hello how r u?'),
(7, 'Raman Senthuran', 'rsenthu123@gmail.com', 'hi', 'This is reagesd fslkfodf slasdfjsdofn dfjsdfsdfj asdfj'),
(8, 'George', 'george@hotmail.com', 'hi', 'hellooooooooooooooooooo');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(30) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `passportno` varchar(25) DEFAULT NULL,
  `roomtype` enum('Medium','Normal','VIP') DEFAULT NULL,
  `noofrooms` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `price` varchar(100) DEFAULT '0',
  `datein` date DEFAULT NULL,
  `dateend` date DEFAULT NULL,
  `message` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `passportno`, `roomtype`, `noofrooms`, `price`, `datein`, `dateend`, `message`) VALUES
(18, 'DDDD', 'dd@gmail.com', '124235235', '32523523', 'VIP', '2', '3253', '2018-02-06', '2017-02-01', 'asdvfsdgbvds'),
(22, 'Thamays', 'thamays@gmail.com', '53345', '2525', 'VIP', '4', '3523', '2017-02-09', '2017-02-10', '35efasv');

-- --------------------------------------------------------

--
-- Table structure for table `user_registration`
--

CREATE TABLE `user_registration` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_registration`
--

INSERT INTO `user_registration` (`id`, `name`, `email`, `password`) VALUES
(1, 'thuvaragan', 'rthuvaragan@gmail.com', 'd9f322a10ea7aa845c5a4c9ee9d8afa0'),
(2, 'shafraz', 'safras@gmail.com', '68053af2923e00204c3ca7c6a3150cf7'),
(13, 'sanjee', 'sanjee', '68053af2923e00204c3ca7c6a3150cf7'),
(14, 'dilshard', 'dilshard@gmail.com', '202cb962ac59075b964b07152d234b70'),
(15, 'haran', 'haran@gmail.com', '68053af2923e00204c3ca7c6a3150cf7'),
(16, 'jude', 'jude@gmail.com', '71b3b26aaa319e0cdf6fdb8429c112b0'),
(18, 'thuwaragan', 'rthuva@yahoo.com', '68053af2923e00204c3ca7c6a3150cf7'),
(19, 'Mazin', 'A.Mazin@wlv.ac.uk', '133987b0b6ad0c01fc0ccbdae1b95449'),
(20, 'ahamed', 'ahamed@yahoo.com', '250cf8b51c773f3f8dc8b4be867a9a02'),
(21, 'Nasleem', 'nasleem@yahoo.com', '68053af2923e00204c3ca7c6a3150cf7'),
(22, 'san', 'DSD', '68053af2923e00204c3ca7c6a3150cf7'),
(23, 'Nanthan', 'nantha@gmail.com', '71b3b26aaa319e0cdf6fdb8429c112b0'),
(24, 'AAAAAAA', 'Aa@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(25, 'Thamays', 'thamays@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_registration`
--
ALTER TABLE `user_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user_registration`
--
ALTER TABLE `user_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
